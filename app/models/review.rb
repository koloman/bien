class Review < ApplicationRecord

  validates :title, length: { in: 3...255 }
  validates :body, length: { minimum: 10 }
  validates :restaurant, length: { in: 3...255 }
  validates :score, numericality: { only_integer: true, in: 0..10 }

  def to_param
    id.to_s + '-' + title.parameterize(separator: '_')
  end

end
