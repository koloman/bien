class ReviewsController < ApplicationController
  
  def index
    # list all of our reviews
    @number = rand(100)

    # retrieve all review from the db
    @reviews = Review.all    
  end

  def new
    # form for adding new review
    @review = Review.new
  end

  def create
    # take info from the form and add it to the model
    @review = Review.new(review_params)

    # we want to check if the model can be saved
    # if it is, we'll redirect to the home page
    # if it isn't, show the new form
    if @review.save
      redirect_to review_path(@review)
    else
      # show the view for the new.html.erb, with all info
      render action: :new, status: :unprocessable_entity
    end
  end

  def show
    # find individual review from db
    @review = Review.find(params[:id])
  end

  def destroy
    # find the individual review
    @review = Review.find(params[:id])
    # destroy it
    @review.destroy
    # redirect to homepage
    redirect_to root_path
  end

  def edit
    @review = Review.find(params[:id])
  end

  def update
    # find the review
    @review = Review.find(params[:id])
    # update values
    if @review.update(review_params)
      # redirect to review page
      redirect_to review_path(@review)
    else
      render action: :edit, status: :unprocessable_entity
    end
  end

  private

  def review_params
    params.require(:review)
      .permit(:title, :body, :score, :restaurant, :ambiance)
  end

end
